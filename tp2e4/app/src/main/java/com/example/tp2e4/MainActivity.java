package com.example.tp2e4;


import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener{


    private LinearLayout myLayout1;
    private SensorManager sensorMgr;
    private Sensor sensor;
    private TextView tvI;

    private long lastUpdate = 0;
    private float x, y, last_x, last_y, delta_x, delta_y;
    private String direction;
    private static final int SHAKE_THRESHOLD = 600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.myLayout1 = (LinearLayout) findViewById(R.id.layout1);
        sensorMgr = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorMgr.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);

        TextView tvPrompt = new TextView(this);
        tvPrompt.setText("Direction de déplacement du tél :");
        myLayout1.addView(tvPrompt);
        tvI=new TextView(this);
        myLayout1.addView(tvI);
        last_x = 0; last_y = 0;

    }

    public void onSensorChanged(SensorEvent event){
        long curTime = System.currentTimeMillis();
        if((curTime - lastUpdate) > 200) {
            lastUpdate=curTime;
            direction = "";
            x = event.values[0];
            y = event.values[1];
            delta_x = last_x - x;
            delta_y = last_y - y;
            last_x = x;
            last_y = y;

            if (delta_x > 1) {
                direction += "Droite ";
            }
            if (delta_x < -1) {
                direction += "Gauche ";
            }
            if (delta_y > 1) {
                direction += "Bas";
            }
            if (delta_y < -1) {
                direction += "Haut";
            }
            tvI.setText(direction);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    protected void onPause() {
        super.onPause();
        sensorMgr.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        sensorMgr.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI);
    }


}