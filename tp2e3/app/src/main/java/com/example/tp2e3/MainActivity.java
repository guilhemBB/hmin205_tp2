package com.example.tp2e3;


import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.LinearLayout;


public class MainActivity extends AppCompatActivity implements SensorEventListener{


    private LinearLayout myLayout1;
    private SensorManager sensorMgr;
    private Sensor sensor;

    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.myLayout1 = (LinearLayout) findViewById(R.id.layout1);
        sensorMgr = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorMgr.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI);

    }

    public void onSensorChanged(SensorEvent event){
        Sensor mySensor = event.sensor;

        if(mySensor.getType() == Sensor.TYPE_ACCELEROMETER){
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            long curTime = System.currentTimeMillis();

            if((curTime - lastUpdate) > 100){

                //TextView tvI = new TextView(this); //pour savoir ce qui est une valeur faible ou forte

                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                // float speed = (float)Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2)); //Cette mesure retourne 9,81 soit l'acceleration et non la vitesse
                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;

                if (speed > 300){
                    myLayout1.setBackgroundColor(Color.RED);
                }
                else myLayout1.setBackgroundColor(Color.BLACK);
                if (speed < 50){
                    myLayout1.setBackgroundColor(Color.GREEN);
                }
                //tvI.setText(" "+speed); //pour savoir ce qui est une valeur faible ou forte
                //myLayout1.addView(tvI);
                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    protected void onPause() {
        super.onPause();
        sensorMgr.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        sensorMgr.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI);
    }


}