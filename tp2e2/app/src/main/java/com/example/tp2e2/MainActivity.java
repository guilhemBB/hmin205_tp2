package com.example.tp2e2;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    private LinearLayout myLayout1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.myLayout1 = (LinearLayout) findViewById(R.id.layout1);
        ArrayList<String> AL_listeCapteurs = new ArrayList<>();

        SensorManager senMan = (SensorManager) getSystemService(SENSOR_SERVICE);
        if(senMan.getDefaultSensor(Sensor.TYPE_HEART_BEAT) != null){
            AL_listeCapteurs.add("Votre appareil dispose d'un cardiofrequencemètre");
        }
        else{
            AL_listeCapteurs.add("Votre appareil ne dispose pas de cardiofrequencemètre, imposible de suivre votre rythme cardique");
        }

        if(senMan.getDefaultSensor(Sensor.TYPE_PRESSURE) != null){
            AL_listeCapteurs.add("Votre appareil dispose d'un capteur de pression");
        }
        else{
            AL_listeCapteurs.add("Votre appreil ne dispose pas de capteur de pression, ce type de mesure est impossible");
        }


        ListView LV_listeCapteurs = new ListView(this);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,AL_listeCapteurs);
        LV_listeCapteurs.setAdapter(arrayAdapter);
        myLayout1.addView(LV_listeCapteurs);

    }
}