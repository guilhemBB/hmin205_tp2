package com.example.tp2e5;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.widget.LinearLayout;


public class MainActivity extends AppCompatActivity implements SensorEventListener{


    private LinearLayout myLayout1;
    private SensorManager sensorMgr;
    private Sensor sensor;
    private boolean FlashOn = false;
    private CameraManager mCameraManager;

    private long lastUpdate = 0;
    private static final int SHAKE_THRESHOLD = 600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.myLayout1 = (LinearLayout) findViewById(R.id.layout1);
        sensorMgr = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorMgr.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);

        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        long curTime = System.currentTimeMillis();
        if((curTime - lastUpdate) > 1000 && event.values[0] > 5 && event.values[1] > 5) {
            lastUpdate=curTime;
            switchFlash(mCameraManager);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    protected void onPause() {
        super.onPause();
        sensorMgr.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        sensorMgr.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI);
    }

    private void switchFlash(CameraManager mCameraManager){
        if(FlashOn){
            try {
                mCameraManager.setTorchMode(mCameraManager.getCameraIdList()[0], false);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
            FlashOn = false;
        }

        else{
            try {
                mCameraManager.setTorchMode(mCameraManager.getCameraIdList()[0], true);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
            FlashOn = true;
        }
    }


}