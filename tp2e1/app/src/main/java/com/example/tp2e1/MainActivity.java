package com.example.tp2e1;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private LinearLayout myLayout1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.myLayout1 = (LinearLayout) findViewById(R.id.layout1);
        ArrayList<String> AL_listeCapteurs = new ArrayList<>();

        SensorManager senMan = (SensorManager) getSystemService(SENSOR_SERVICE);
        List<Sensor> sensors = senMan.getSensorList(Sensor.TYPE_ALL);
        for(Sensor sensor : sensors){
            AL_listeCapteurs.add("Le capteur "+sensor.getName()+" est de type "+sensor.getStringType());
        }

        ListView LV_listeCapteurs = new ListView(this);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,AL_listeCapteurs);
        LV_listeCapteurs.setAdapter(arrayAdapter);
        myLayout1.addView(LV_listeCapteurs);

    }
}