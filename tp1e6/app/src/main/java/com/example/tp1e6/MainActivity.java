package com.example.tp1e6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener{

    private LinearLayout myLayout1;
    private SensorManager sensorMgr;
    private Sensor sensorProx;
    private TextView tvDist;
    private ImageView fond;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.myLayout1 = (LinearLayout) findViewById(R.id.layout1);

        sensorMgr = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorProx = sensorMgr.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        sensorMgr.registerListener((SensorEventListener) this,sensorProx,SensorManager.SENSOR_DELAY_GAME);

        tvDist = new TextView(this);
        myLayout1.addView(tvDist);

        fond = new ImageView(this);
        myLayout1.addView(fond);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.values[0]==0){
            tvDist.setText("Proche");
            fond.setImageResource(R.drawable.proche);
        }
        else{
            tvDist.setText("Lointain");
            fond.setImageResource(R.drawable.loin);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    protected void onPause(){
        super.onPause();
        sensorMgr.unregisterListener(this);
    }

    protected void onResume(){
        super.onResume();
        sensorMgr.registerListener(this,sensorProx,sensorMgr.SENSOR_DELAY_GAME);
    }
}